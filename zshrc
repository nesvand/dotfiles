unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    *)          machine="UNKNOWN:${unameOut}"
esac
# echo ${machine}

if [ machine = "Mac" ]; then
    # Mac
    export PATH="$HOME/bin:/usr/local/bin:/usr/local/sbin:$PATH"
    export PATH="$HOME/.cargo/bin:$PATH"
    export PATH="$HOME/Library/Python/3.9/bin:$PATH"
    export PATH="$HOME/Library/Android/sdk/platform-tools:$PATH"
else
    # Linux
fi


# Prevent using something silly like nano
export EDITOR=nvim


alias lx="exa -lahg --group-directories-first --icons"


# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


if [[ -r $HOME/.local/share/.personal-access-token ]]; then
    source $HOME/.local/share/.personal-access-token
fi


# True if $1 is an executable in $PATH
function is_bin_in_path {
	builtin whence -p "$1" &> /dev/null
}


# antigen - zsh plugin manager
source $HOME/antigen/antigen.zsh
antigen init $HOME/.antigenrc
unalias gr # not needed and collides with https://github.com/mixu/gr

fpath=(${ASDF_DIR}/completions $fpath)
autoload -Uz compinit && compinit

if [[ -d $HOME/go ]]; then
    export GOPATH="$HOME/go"; export GOROOT="$(asdf where golang)/go"; export PATH="$GOPATH/bin:$PATH";
    export GO111MODULE="auto"
fi


# p10k - zsh theme
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh


# NEDS dev path export (must be after `g` to ensure $GOPATH is set)
export NEDS="$GOPATH/src/git.neds.sh"
export UI="$NEDS/technology/code/ui"
export USVC="$NEDS/usvc"
export TOOL="$NEDS/technology/code/tool"
export API="$NEDS/technology/code/api"
export SRV="$NEDS/technology/code/srv"
export WEB="$NEDS/technology/code/web"
export LADS="$GOPATH/src/gitlab.ladbrokes.net.au"
# support for mre - MR Extractor
export PATH=$PATH:$NEDS/jamie.corrigan/mre/bin
# change default role for `nctx` from administrator
export NCTX_ROLE="power-user"
export GOFLAGS="-tags=noaws"
export NO_VAULT="pls"
export MYSQL_MAX_CONNECTIONS=301 # increase the number of connections available without going too crazy
# stackz flags
export STACKZ_MYSQL=brew
export STACKZ_CONSUL=asdf
export STACKZ_REDIS=asdf
export STACKZ_ELASTICSEARCH=asdf


# Adds basic completions for `stackz
STACKZ_COMPLETIONS=$TOOL/stackz/scripts/stackz-completions.sh
if [[ -f "$STACKZ_COMPLETIONS" ]]; then
    . $STACKZ_COMPLETIONS
fi


# https://github.com/nvbn/thefuck
if type "thefuck" > /dev/null; then
    eval $(thefuck --alias)
fi


alias ci-pass='go fmt ./... && golint ./... && protolint lint --config_path=$GOPATH/src/git.neds.sh/technology/guidelines/build/backend/.protolint.yaml proto/**/*.proto && go test ./... && golangci-lint run --fast --config $GOPATH/src/git.neds.sh/technology/guidelines/build/backend/.golangci.yml --new-from-rev=origin/master ./...'

export NVM_DIR="$HOME/.config/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


function prompt_my_nctx_env() {
    local color='%F{green}'
    local current;
    current=$(kubectl config current-context | awk -F/ '{print $1}');
    local nenv;
    nenv=$(echo "$current" | awk -F- '{print $1}');
    local symbol='\u2602';

    if [ "$nenv" = "uat" ]; then
        color='%F{blue}';
    elif [ "$nenv" = "production" ]; then
        color='%F{red}' ;
    fi

    local bcolor='%F{white}';
    local brand;
    brand=$(echo "$current" | awk -F- '{print $2}')

    if [ "$brand" = "neds" ]; then
        bcolor='%F{darkorange}' ;
    elif [ "$brand" = "ladbrokes" ]; then
        bcolor='%F{red}' ;
    elif [ "$brand" = "bookmaker" ]; then
        bcolor='%F{grey}' ;
    elif [ "$brand" = "betstar" ]; then
        bcolor='%F{blue}' ;
    elif [ "$brand" = "core" ]; then
        bcolor='%F{green}' ;
    fi

    print -n "%{$color%}$symbol $nenv-%{$bcolor%}$brand";
}

fpath+=${ZDOTDIR:-~}/.zsh_functions


qq() {
    clear

    logpath="$TMPDIR/q"
    if [[ -z "$TMPDIR" ]]; then
        logpath="/tmp/q"
    fi

    if [[ ! -f "$logpath" ]]; then
        echo 'Q LOG' > "$logpath"
    fi

    tail -100f -- "$logpath"
}

rmqq() {
    logpath="$TMPDIR/q"
    if [[ -z "$TMPDIR" ]]; then
        logpath="/tmp/q"
    fi
    if [[ -f "$logpath" ]]; then
        rm "$logpath"
    fi
    qq
}

