export PATH="$HOME/.local/bin:$PATH"
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    *)          machine="UNKNOWN:${unameOut}"
esac
# echo ${machine}

if [ ${machine} = "Linux" ]; then
    if [ -d "$HOME/.cargo" ]; then
        . "$HOME/.cargo/env"
    fi

    export PATH="/usr/local/bin:/usr/local/sbin:$PATH"

    # XDG Config
    export XDG_CONFIG_HOME="$HOME/.config"
    export XDG_CACHE_HOME="$HOME/.cache"
    export XDG_DATA_HOME="$HOME/.local/share"
    export XDG_DATA_DIRS="/var/lib/flatpak/exports/share:$XDG_DATA_HOME/flatpak/exports/share:$XDG_DATA_DIRS" # Flatpak compatibility

    LINUXBREW=/home/linuxbrew/.linuxbrew/bin/brew
    if [[ -f "$LINUXBREW" ]]; then
        eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
    fi
fi
